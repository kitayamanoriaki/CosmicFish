# Italian messages for numdiff
# Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013  Ivano Primi <ivprimi@libero.it>
# This file is distributed under the same license as the Numdiff program.
# Ivano Primi <ivprimi@libero.it>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: numdiff 5.8.1\n"
"Report-Msgid-Bugs-To: ivprimi@libero.it\n"
"POT-Creation-Date: 2013-08-07 15:35+0200\n"
"PO-Revision-Date: 2013-09-15 17:43+0200\n"
"Last-Translator: Ivano Primi <ivprimi@libero.it>\n"
"Language-Team: ITALIAN <ivprimi@libero.it>\n"
"Language: Italian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1 ? 1 : 0;\n"

#: cmpfns.c:277 cmpfns.c:293 cmpfns.c:347 cmpfns.c:363
#, c-format
msgid ""
"@ Line %lu in file \"%s\"\n"
"  contains too many fields to be properly processed!\n"
msgstr ""
"@ La linea %lu nel file \"%s\"\n"
"  contiene troppi campi per essere elaborata correttamente!\n"

#: cmpfns.c:379 cmpfns.c:395
#, c-format
msgid "@ Line %lu in file \"%s\" is shorter than expected!\n"
msgstr "@ La linea %lu nel file \"%s\" è più corta del previsto\n"

#: cmpfns.c:531 cmpfns.c:556 cmpfns.c:589 cmpfns.c:608
#, c-format
msgid ""
"\n"
"***  File \"%s\" is binary,\n"
"***  cannot read from it\n"
msgstr ""
"\n"
"***  Il file \"%s\" è un file binario,\n"
"***  non è possibile leggere dati da esso\n"

#: cmpfns.c:534 cmpfns.c:559 cmpfns.c:592 cmpfns.c:613
#, c-format
msgid ""
"\n"
"***  Error while reading from file \"%s\"\n"
msgstr ""
"\n"
"***  Errore in lettura dal file \"%s\"\n"

#: cmpfns.c:537 cmpfns.c:562 cmpfns.c:595 cmpfns.c:618
#, c-format
msgid ""
"\n"
"***  Out of memory while reading from file \"%s\"\n"
msgstr ""
"\n"
"***  Esaurimento di memoria durante la lettura del file \"%s\"\n"

#: cmpfns.c:544 cmpfns.c:567 cmpfns.c:574 cmpfns.c:599
#, c-format
msgid ""
"\n"
"***  End of file \"%s\" reached while trying to read line %lu.\n"
msgstr ""
"\n"
"***  Raggiunta la fine del file \"%s\"\n"
"***  mentre si cercava di leggere la linea %lu.\n"

#: cmpfns.c:545 cmpfns.c:568 cmpfns.c:575 cmpfns.c:600
#, c-format
msgid "***  File \"%s\" has more lines than file \"%s\",\n"
msgstr "***  Il file \"%s\" ha più linee del file \"%s\",\n"

#: cmpfns.c:547 cmpfns.c:570 cmpfns.c:577 cmpfns.c:602
#, c-format
msgid ""
"***  line %lu is the last one read from file \"%s\"\n"
"\n"
msgstr ""
"***  la linea %lu è l'ultima letta dal file \"%s\"\n"
"\n"

#: error.c:125
msgid "Unknown system error"
msgstr "Errore di sistema sconosciuto"

#: errors.c:43
#, c-format
msgid ""
"%s: insufficient memory for new allocation,\n"
"the execution of the program ends now\n"
msgstr ""
"%s: memoria insufficiente per nuova assegnazione,\n"
"l'esecuzione del programma finisce qui\n"

#: errors.c:76
#, c-format
msgid "Runtime error: %s\n"
msgstr "Errore durante l'esecuzione del programma: %s\n"

#: errors.c:110
#, c-format
msgid "Runtime warning: %s\n"
msgstr ""
"Avvertimento lanciato durante l'esecuzione del programma:\n"
"%s\n"

#: flags.c:58
msgid "<The array is empty>\n"
msgstr "<L'array è vuoto>\n"

#: getopt.c:551 getopt.c:570
#, c-format
msgid "%s: option `%s' is ambiguous\n"
msgstr "%s: l'opzione `%s' è ambigua\n"

#: getopt.c:603 getopt.c:607
#, c-format
msgid "%s: option `--%s' doesn't allow an argument\n"
msgstr "%s: l'opzione `--%s' non vuole argomenti\n"

#: getopt.c:616 getopt.c:621
#, c-format
msgid "%s: option `%c%s' doesn't allow an argument\n"
msgstr "%s: l'opzione `%c%s' non vuole argomenti\n"

#: getopt.c:667 getopt.c:689 getopt.c:1020 getopt.c:1042
#, c-format
msgid "%s: option `%s' requires an argument\n"
msgstr "%s: l'opzione `%s' ha bisogno di un argomento\n"

#: getopt.c:727 getopt.c:730
#, c-format
msgid "%s: unrecognized option `--%s'\n"
msgstr "%s: l'opzione `--%s' risulta sconosciuta\n"

#: getopt.c:738 getopt.c:741
#, c-format
msgid "%s: unrecognized option `%c%s'\n"
msgstr "%s: l'opzione `%c%s' risulta sconosciuta\n"

#: getopt.c:796 getopt.c:799
#, c-format
msgid "%s: illegal option -- %c\n"
msgstr "%s: opzione inammissibile -- %c\n"

#: getopt.c:805 getopt.c:808
#, c-format
msgid "%s: invalid option -- %c\n"
msgstr "%s: opzione non valida -- %c\n"

#: getopt.c:863 getopt.c:882 getopt.c:1095 getopt.c:1116
#, c-format
msgid "%s: option requires an argument -- %c\n"
msgstr "%s: opzione con argomento obbligatorio -- %c\n"

#: getopt.c:935 getopt.c:954
#, c-format
msgid "%s: option `-W %s' is ambiguous\n"
msgstr "%s: l'opzione `-W %s' è ambigua\n"

#: getopt.c:978 getopt.c:999
#, c-format
msgid "%s: option `-W %s' doesn't allow an argument\n"
msgstr "%s: l'opzione `-W %s' non vuole argomenti\n"

#: inout.c:347 util.c:121 util.c:136 util.c:242 util.c:258
#, c-format
msgid ""
"***  Fatal error occurred in function %s:\n"
"%s"
msgstr ""
"***  Errore non emendabile verificatosi durante\n"
"***  l'esecuzione della funzione %s:\n"
"%s"

#: inout.c:349 util.c:123 util.c:138 util.c:244 util.c:260
msgid ""
"***  a very long line has been encountered which contains\n"
"***  too many fields to be correctly handled\n"
msgstr ""
"***  È stata incontrata una linea molto lunga che contiene\n"
"***  troppi campi per poter essere elaborata correttamente\n"

#: io.c:127
msgid "@ Absolute error = "
msgstr "@ Errore assoluto = "

#: io.c:129
msgid ", Relative error = "
msgstr ", Errore relativo = "

#: main.c:312
msgid ""
"\n"
"  In the computation of the following quantities\n"
"  only the errors with positive sign are considered:\n"
msgstr ""
"\n"
"  Nel calcolo dei seguenti valori vengono considerati\n"
"  solo gli errori con segno positivo:\n"

#: main.c:314
msgid ""
"  differences due to numeric fields of the second file that are\n"
"  less than the corresponding fields in the first file are neglected\n"
"\n"
msgstr ""
"  le differenze dovute a campi numerici del secondo file\n"
"  che risultano essere minori dei corrispondenti campi\n"
"  del primo file sono state tralasciate\n"
"\n"

#: main.c:319
msgid ""
"\n"
"  In the computation of the following quantities\n"
"  only the errors with negative sign are considered:\n"
msgstr ""
"\n"
"  Nel calcolo dei seguenti valori vengono considerati\n"
"  solo gli errori con segno negativo:\n"

#: main.c:321
msgid ""
"  differences due to numeric fields of the second file that are\n"
"  greater than the corresponding fields in the first file are neglected\n"
"\n"
msgstr ""
"  le differenze dovute a campi numerici del secondo file\n"
"  che risultano essere maggiori dei corrispondenti campi\n"
"  del primo file sono state tralasciate\n"
"\n"

#: main.c:327
msgid ""
"\n"
"No numeric comparison has been done\n"
msgstr ""
"\n"
"Non è stato effettuato alcun confronto numerico\n"

#: main.c:331
#, c-format
msgid ""
"\n"
"One numeric comparison has been done and\n"
"the resulting numeric difference is negligible\n"
msgid_plural ""
"\n"
"%d numeric comparisons have been done and\n"
"the resulting numeric differences are all negligible\n"
msgstr[0] ""
"\n"
"È stato effettuato un solo confronto numerico e\n"
"la differenza numerica rilevata è trascurabile\n"
msgstr[1] ""
"\n"
"Sono stati effettuati %d confronti numerici e\n"
"le differenze numeriche rilevate sono tutte trascurabili\n"

#: main.c:338
#, c-format
msgid ""
"\n"
"One numeric comparison has been done and\n"
"has produced an outcome beyond the tolerance threshold\n"
msgid_plural ""
"\n"
"%d numeric comparisons have been done, all of them\n"
"have produced an outcome beyond the tolerance threshold\n"
msgstr[0] ""
"\n"
"È stato effettuato un solo confronto numerico e\n"
"ha prodotto un risultato oltre la soglia di tolleranza\n"
msgstr[1] ""
"\n"
"Sono stati effettuati %d confronti numerici e tutti\n"
"hanno prodotto un risultato oltre la soglia di tolleranza\n"

#: main.c:346
#, c-format
msgid ""
"\n"
"One numeric comparison has been done,\n"
msgid_plural ""
"\n"
"%d numeric comparisons have been done,\n"
msgstr[0] ""
"\n"
"È stato effettuato un solo confronto numerico,\n"
msgstr[1] ""
"\n"
"Sono stati effettuati %d confronti numerici,\n"

#: main.c:351
#, c-format
msgid ""
"only one numeric comparison has produced an outcome\n"
"beyond the tolerance threshold\n"
msgid_plural ""
"%d numeric comparisons have produced an outcome\n"
"beyond the tolerance threshold\n"
msgstr[0] ""
"un solo confronto numerico ha prodotto un risultato\n"
"oltre la soglia di tolleranza\n"
msgstr[1] ""
"%d confronti numerici hanno prodotto un risultato\n"
"oltre la soglia di tolleranza\n"

#: main.c:356
msgid ""
"\n"
"Largest absolute error in the set of relevant numerical differences:\n"
msgstr ""
"\n"
"Massimo errore assoluto nell'insieme delle differenze numeriche rilevanti:\n"

#: main.c:359
msgid ""
"\n"
"Corresponding relative error:\n"
msgstr ""
"\n"
"Corrispondente errore relativo:\n"

#: main.c:362
msgid ""
"\n"
"Largest relative error in the set of relevant numerical differences:\n"
msgstr ""
"\n"
"Massimo errore relativo nell'insieme delle differenze numeriche rilevanti:\n"

#: main.c:365
msgid ""
"\n"
"Corresponding absolute error:\n"
msgstr ""
"\n"
"Corrispondente errore assoluto:\n"

#: main.c:369
msgid ""
"\n"
"Sum of all absolute errors:\n"
msgstr ""
"\n"
"Somma degli errori assoluti:\n"

#: main.c:372
msgid ""
"\n"
"Sum of the relevant absolute errors:\n"
msgstr ""
"\n"
"Somma degli errori assoluti non trascurabili:\n"

#: main.c:378
msgid ""
"\n"
"Arithmetic mean of all absolute errors:\n"
msgstr ""
"\n"
"Media aritmetica degli errori assoluti:\n"

#: main.c:381
msgid ""
"\n"
"Arithmetic mean of the relevant absolute errors:\n"
msgstr ""
"\n"
"Media aritmetica degli errori assoluti non trascurabili:\n"

#: main.c:390
msgid ""
"\n"
"Square root of the sum of the squares of all absolute errors:\n"
msgstr ""
"\n"
"Radice quadrata della somma dei quadrati\n"
"di tutti gli errori assoluti:\n"

#: main.c:393
msgid ""
"\n"
"Quadratic mean of all absolute errors:\n"
msgstr ""
"\n"
"Media quadratica degli errori assoluti:\n"

#: main.c:401
msgid ""
"\n"
"Square root of the sum of the squares\n"
"of the relevant absolute errors:\n"
msgstr ""
"\n"
"Radice quadrata della somma dei quadrati\n"
"di tutti gli errori assoluti non trascurabili:\n"

#: main.c:404
msgid ""
"\n"
"Quadratic mean of the relevant absolute errors:\n"
msgstr ""
"\n"
"Media quadratica degli errori assoluti non trascurabili:\n"

#: main.c:428
#, c-format
msgid "***  %s: memory exhausted\n"
msgstr "***  %s: memoria esaurita\n"

#: main.c:494
msgid ""
"\n"
"***  The requested comparison cannot be performed:\n"
msgstr ""
"\n"
"***  Il confronto richiesto non può essere effettuato:\n"

#: main.c:495
#, c-format
msgid "***  At least one between \"%s\" and \"%s\" is a binary file\n"
msgstr "***  Almeno uno tra \"%s\" e \"%s\" è un file binario\n"

#: main.c:521
#, c-format
msgid ""
"\n"
"+++  Files \"%s\" and \"%s\" have the same structure\n"
msgstr ""
"\n"
"+++  I file \"%s\" e \"%s\" hanno la stessa struttura\n"

#: main.c:524
#, c-format
msgid ""
"\n"
"+++  Files \"%s\" and \"%s\" are equal\n"
msgstr ""
"\n"
"+++  I file \"%s\" e \"%s\" sono uguali\n"

#: main.c:528
#, c-format
msgid ""
"\n"
"+++  File \"%s\" differs from file \"%s\"\n"
msgstr ""
"\n"
"+++  Il file \"%s\" differisce dal file \"%s\"\n"

#: new.c:151 numutil.c:157 numutil.c:312
#, c-format
msgid ""
"%s: a number with a too small exponent has been found,\n"
"namely \"%s\".\n"
msgstr ""
"%s: è stato trovato un numero con un esponente troppo basso,\n"
"precisamente \"%s\".\n"

#: new.c:152 numutil.c:158 numutil.c:313
#, c-format
msgid "Exponents smaller than %ld are not accepted,\n"
msgstr "Esponenti inferiori a %ld non sono accettati,\n"

#: new.c:153 new.c:160 numutil.c:159 numutil.c:166 numutil.c:314 numutil.c:321
#, c-format
msgid "the execution of the program ends now\n"
msgstr "l'esecuzione del programma finisce qui\n"

#: new.c:158 numutil.c:164 numutil.c:319
#, c-format
msgid ""
"%s: a number with a too large exponent has been found,\n"
"namely \"%s\".\n"
msgstr ""
"%s: è stato trovato un numero con un esponente troppo alto,\n"
"precisamente \"%s\".\n"

#: new.c:159 numutil.c:165 numutil.c:320
#, c-format
msgid "Exponents larger than %ld are not accepted,\n"
msgstr "Esponenti superiori a %ld non sono accettati,\n"

#: number.c:1228
msgid "power with non integral base"
msgstr "potenza con base non intera"

#: number.c:1234 number.c:1285
msgid "power with non integral exponent"
msgstr "potenza con esponente non intero"

#: number.c:1241
msgid "modulus is not an integral value"
msgstr "il modulo non è un valore intero"

#: number.c:1289
msgid "exponent too large in raise"
msgstr "potenza con esponente troppo grande"

#: numutil.c:863
#, c-format
msgid ""
"The string \"%s\"\n"
"is not a valid number, the execution of the program ends now\n"
msgstr ""
"La stringa \"%s\"\n"
"non è un numero valido, l'esecuzione del programma finisce qui\n"

#: options.c:46
msgid "Ivano Primi"
msgstr "Ivano Primi"

#: options.c:47
#, c-format
msgid ""
"License GPLv3+: GNU GPL version 3 or later,\n"
"see <http://gnu.org/licenses/gpl.html>.\n"
"This is free software: you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law.\n"
msgstr ""
"Licenza GPLv3+: GNU GPL versione 3 o successiva,\n"
"vedi <http://gnu.org/licenses/gpl.html>.\n"
"Questo è software libero: sei libero di modificarlo e redistribuirlo.\n"
"NON c'è NESSUNA GARANZIA, per quanto consentito dalle vigenti normative.\n"

#: options.c:53
msgid ""
"The software has been linked against\n"
"the GNU Multiple Precision Arithmetic Library,\n"
"version number"
msgstr ""
"Il software è stato linkato alla libreria\n"
"GNU per l'aritmetica a precisione multipla (GNU MP),\n"
"numero di versione"

#: options.c:57
msgid ""
"The software has been built with\n"
"its own internal support for multiple precision arithmetic"
msgstr ""
"Il software è stato compilato attivando\n"
"il suo supporto interno per l'aritmetica a precisione multipla"

#: options.c:64
msgid "Usage:"
msgstr "Uso:"

#: options.c:65
msgid "or"
msgstr "oppure"

#: options.c:67
#, c-format
msgid ""
"\n"
"Compare putatively similar files line by line and field by field,\n"
"ignoring small numeric differences or/and different numeric formats.\n"
"\n"
msgstr ""
"\n"
"Confronta file grossolanamente simili linea per linea e campo per campo,\n"
"ignorando piccole differenze numeriche o/e formati numerici differenti.\n"
"\n"

#: options.c:68
#, c-format
msgid ""
"RANGE, RANGE1 and RANGE2 stay for a positive integer value or\n"
"for a range of integer values, like 1-, 3-5 or -7.\n"
msgstr ""
"RANGE, RANGE1 e RANGE2 stanno per un valore intero positivo oppure\n"
"per un intervallo di valori interi, per esempio 1-, 3-5 or -7.\n"

#: options.c:70
msgid ""
"The two arguments after the options are the names of the files to compare."
msgstr "I due argomenti dopo le opzioni sono i nomi dei file da confrontare."

#: options.c:71
msgid ""
"The complete paths of the files should be given,\n"
"a directory name is not accepted."
msgstr ""
"È bene fornire i percorsi completi dei file,\n"
"un nome di cartella non viene accettato."

#: options.c:72
msgid ""
"The given paths cannot refer to the same file\n"
"but one of them can be \"-\", which refers to stdin."
msgstr ""
"I file non possono coincidere ma uno di loro può essere \"-\",\n"
"che si riferisce allo standard input."

#: options.c:73
msgid ""
"Exit status: 1 if files differ, 0 if they are equal, -1 (255) in case of "
"error"
msgstr ""
"Codice di uscita: 1 se i file sono diversi, 0 se uguali, -1 (255) su errore"

#: options.c:76
msgid ""
"Specify the set of characters to use as delimiters\n"
"    while splitting the input lines into fields"
msgstr ""
"Specifica l'insieme dei caratteri da usare come separatori\n"
"    al momento di suddividere le linee di input in campi"

#: options.c:77 options.c:81
msgid "(The default set of delimiters is space, tab and newline)."
msgstr "(L'insieme predefinito è spazio bianco, tabulazione e nuova linea)."

#: options.c:78
msgid ""
"If IFS is prefixed with 1: or 2: then use the given delimiter set\n"
"    only for the lines from the first or the second file respectively"
msgstr ""
"Se IFS è preceduto dal prefisso 1: o da 2: allora usa l'insieme di\n"
"    caratteri specificato solo per le linee del primo ovvero del secondo file"

#: options.c:80
msgid ""
"Specify the set of strings to use as delimiters\n"
"    while splitting the input lines into fields"
msgstr ""
"Specifica l'insieme delle stringhe da usare come separatori\n"
"    al momento di suddividere le linee di input in campi"

#: options.c:82
msgid ""
"If DELIMS is prefixed with 1: or 2: then use the given delimiter set\n"
"    only for the lines from the first or the second file respectively"
msgstr ""
"Se DELIMS è preceduto dal prefisso 1: o da 2: allora usa l'insieme di\n"
"    stringhe specificato solo per le linee del primo ovvero del secondo file"

#: options.c:84
msgid ""
"Set to THRVAL the maximum absolute difference permitted\n"
"    before that two numeric fields are regarded as different\n"
"    (The default value is zero)."
msgstr ""
"Imposta al valore THRVAL la massima differenza assoluta consentita\n"
"    prima che due campi numerici siano considerati diversi\n"
"    (Il valore predefinito è zero)."

#: options.c:85 options.c:89
msgid ""
"If a RANGE is given, use the specified\n"
"    threshold only when comparing fields whose positions lie in RANGE."
msgstr ""
"Se un solo RANGE è dato, usa il valore di soglia specificato\n"
"    solo per confrontare campi le cui posizioni cadono dentro RANGE."

#: options.c:86 options.c:90
msgid ""
"If both RANGE1 and RANGE2 are given and have the same length,\n"
"    then use the specified threshold when comparing a field of FILE1\n"
"    lying in RANGE1 with the corresponding field of FILE2 in RANGE2"
msgstr ""
"Se sono dati sia RANGE1 che RANGE2 ed hanno la stessa lunghezza,\n"
"    usa il valore di soglia specificato quando confronti un campo di FILE1\n"
"    che cade in RANGE1 con il corrispondente campo di FILE2 in RANGE2"

#: options.c:88
msgid ""
"Set to THRVAL the maximum relative difference permitted\n"
"    before that two numeric fields are regarded as different\n"
"    (The default value is zero)."
msgstr ""
"Imposta al valore THRVAL la massima differenza relativa consentita\n"
"    prima che due campi numerici siano considerati diversi\n"
"    (Il valore predefinito è zero)."

#: options.c:92
msgid ""
"Consider two numerical values as equal only if\n"
"    both absolute and relative difference do not exceed\n"
"    the corresponding tolerance threshold"
msgstr ""
"Considera due campi numerici come uguali solo se\n"
"    la differenza assoluta e quella relativa sono entrambe dentro\n"
"    la corrispondente soglia di tolleranza"

#: options.c:94
msgid "Use the formula indicated by NUM to compute the relative errors."
msgstr "Usa la formula indicata da NUM per calcolare gli errori relativi."

#: options.c:95
msgid "If 'NUM' is 0 use the classic formula."
msgstr "Se 'NUM' è 0 usa la formula classica."

#: options.c:96
msgid ""
"If 'NUM' is 1 compute the relative errors by considering\n"
"    the values in FILE1 as sample values."
msgstr ""
"Se 'NUM' è 1 calcola gli errori relativi considerando\n"
"    i valori in FILE1 come valori campione."

#: options.c:97
msgid ""
"If 'NUM' is 2 compute the relative errors by considering\n"
"    the values in FILE2 as sample values."
msgstr ""
"Se 'NUM' è 2 calcola gli errori relativi considerando\n"
"    i valori in FILE2 come valori campione."

#: options.c:99
msgid ""
"Set to NUM the number of digits in the significands\n"
"    used in multiple precision arithmetic"
msgstr ""
"Imposta a NUM il numero di cifre significative\n"
"    usate nell'aritmetica a precisione multipla"

#: options.c:101
msgid ""
"Ignore all differences due to numeric fields of the second file that\n"
"    are less than the corresponding numeric fields in the first file"
msgstr ""
"Ignora tutte le differenze dovute a campi numerici del secondo file\n"
"    che sono minori dei corrispondenti campi numerici nel primo file"

#: options.c:103
msgid ""
"Ignore all differences due to numeric fields of the second file that\n"
"    are greater than the corresponding numeric fields in the first file"
msgstr ""
"Ignora tutte le differenze dovute a campi numerici del secondo file\n"
"    che sono maggiori dei corrispondenti campi numerici nel primo file"

#: options.c:105
msgid "Ignore changes in case while doing literal comparisons"
msgstr ""
"Nel fare confronti letterali\n"
"    ignora le differenze del tipo maiuscolo/minuscolo"

#: options.c:107
msgid "Set to CURRNAME the currency name for the two files to compare."
msgstr "Imposta a CURRNAME il nome della valuta per i due file da confrontare."

#: options.c:108
msgid ""
"CURRNAME must be prefixed with 1: or 2: to specify the\n"
"    currency name only for the first or the second file"
msgstr ""
"CURRNAME deve essere preceduto dal prefisso 1: o 2: per specificare\n"
"    il nome della valuta solo per il primo ovvero per il secondo file"

#: options.c:110
msgid ""
"Specify the characters representing the decimal point\n"
"    in the two files to compare"
msgstr ""
"Specifica i caratteri rappresentanti il punto decimale\n"
"    nei due file da confrontare"

#: options.c:112
msgid ""
"Specify the characters representing the thousands separator\n"
"    in the two files to compare"
msgstr ""
"Specifica i caratteri rappresentanti il separatore delle migliaia\n"
"    nei due file da confrontare"

#: options.c:114
msgid ""
"Specify the number of digits forming each group of thousands\n"
"    in the two files to compare"
msgstr ""
"Specifica il numero di cifre che formano ogni gruppo di migliaia\n"
"    nei due file da confrontare"

#: options.c:116
msgid ""
"Specify the (optional) prefixes for positive values\n"
"    used in the two files to compare"
msgstr ""
"Specifica i prefissi (opzionali) per valori positivi\n"
"    usati nei due file da confrontare"

#: options.c:118
msgid ""
"Specify the prefixes for negative values\n"
"    used in the two files to compare"
msgstr ""
"Specifica i prefissi per valori negativi\n"
"    usati nei due file da confrontare"

#: options.c:120
msgid ""
"Specify the exponent letters\n"
"    used in the two files to compare"
msgstr ""
"Specifica i simboli che precedono l'esponente decimale\n"
"    nei due file da confrontare"

#: options.c:122
msgid ""
"Specify the characters representing the imaginary unit\n"
"    in the two files to compare"
msgstr ""
"Specifica i caratteri rappresentanti l' unità immaginaria\n"
"    nei due file da confrontare"

#: options.c:124
msgid "Select the fields of the first file that have to be ignored"
msgstr "Seleziona i campi del primo file che devono essere ignorati"

#: options.c:126
msgid "Select the fields of the second file that have to be ignored"
msgstr "Seleziona i campi del secondo file che devono essere ignorati"

#: options.c:128
msgid ""
"While printing the differences between the two compared files\n"
"    show only the numerical ones"
msgstr ""
"Nello stampare le differenze tra i due file confrontati\n"
"    mostra solo quelle di tipo numerico"

#: options.c:130
msgid ""
"While printing the differences between the two compared files\n"
"    neglect all the numerical ones (dummy mode)"
msgstr ""
"Nello stampare le differenze tra i due file confrontati\n"
"    trascura tutte quelle di tipo numerico (modalità stupida)"

#: options.c:132
msgid ""
"Suppress all messages concerning the differences discovered\n"
"    in the structures of the two files"
msgstr ""
"Sopprimi tutti i messaggi relativi alle differenze scoperte\n"
"    nelle strutture dei due file"

#: options.c:134
msgid ""
"For every couple of lines which differ in at least one field print\n"
"    an header to show how these lines appear in the two compared files"
msgstr ""
"Per ogni coppia di linee che differiscono in almeno un campo stampa una\n"
"    intestazione per mostrare come esse appaiono nei due file confrontati"

#: options.c:136
msgid ""
"Display a side by side difference listing of the two files\n"
"    showing which lines are present only in one file, which\n"
"    lines are present in both files but with one or more differing fields,\n"
"    and which lines are identical."
msgstr ""
"Stampa un elenco a colonne affiancate delle differenze tra i due file,\n"
"    mostrando quali linee sono presenti solo in un file, quali linee\n"
"    sono presenti in entrambi i file ma con uno o più campi differenti\n"
"    e quali sono identiche."

#: options.c:137 options.c:158
msgid ""
"If 'NUM' is zero or is not specified, output at most 130 columns per line."
msgstr ""
"Se 'NUM' è zero o non è specificato, visualizza al più\n"
"    130 colonne per linea."

#: options.c:138 options.c:159
msgid "If 'NUM' is a positive number, output at most 'NUM' columns per line."
msgstr ""
"Se 'NUM' è un numero positivo, visualizza al più\n"
"    'NUM' colonne per linea."

#: options.c:139 options.c:160
msgid ""
"If 'NUM' is a negative number, do not output common lines\n"
"    and display at most -'NUM' columns per line."
msgstr ""
"Se 'NUM' è un numero negativo, non stampare le linee comuni\n"
"    ai due file e visualizza al più -'NUM' colonne per linea."

#: options.c:141
msgid "Suppress all the standard output"
msgstr "Sopprimi completamente l'output standard del programma"

#: options.c:143
msgid "Add some statistics to the standard output"
msgstr "Aggiungi alcune informazioni di tipo statistico all'output standard"

#: options.c:145
msgid ""
"Select the fields of the first file that have to be\n"
"    blurred during the synchronization procedure\n"
"    only if they turn out to be numeric"
msgstr ""
"Seleziona i campi del primo file che devono essere\n"
"    offuscati durante la procedura di sincronizzazione\n"
"    quando risultano essere di tipo numerico"

#: options.c:147
msgid ""
"Select the fields of the second file that have to be\n"
"    blurred during the synchronization procedure\n"
"    only if they turn out to be numeric"
msgstr ""
"Seleziona i campi del secondo file che devono essere\n"
"    offuscati durante la procedura di sincronizzazione\n"
"    quando risultano essere di tipo numerico"

#: options.c:149
msgid ""
"Select the fields of the first file that have to be\n"
"    unconditionally blurred during the synchronization procedure"
msgstr ""
"Seleziona i campi del primo file che devono essere\n"
"    sempre offuscati durante la procedura di sincronizzazione"

#: options.c:151
msgid ""
"Select the fields of the second file that have to be\n"
"    unconditionally blurred during the synchronization procedure"
msgstr ""
"Seleziona i campi del secondo file che devono essere\n"
"    sempre offuscati durante la procedura di sincronizzazione"

#: options.c:153
msgid "During synchronization try hard to find a smaller set of changes"
msgstr ""
"Durante la sincronizzazione cerca ad ogni costo di trovare\n"
"    una catena minimale di modifiche"

#: options.c:155
msgid ""
"During synchronization assume large files and\n"
"    many scattered small changes"
msgstr ""
"Durante la sincronizzazione assumi di lavorare su file grandi\n"
"    con tante piccole differenze sparpagliate"

#: options.c:157
msgid ""
"Run only the filter and then show the results of its\n"
"    attempt to synchronize the two files."
msgstr ""
"Esegui solo il filtraggio e quindi mostra i risultati del\n"
"    suo tentativo di sincronizzare i due file."

#: options.c:162
msgid ""
"Expand tabs to spaces in output while displaying the results of the\n"
"    synchronization procedure (meaningful only together with option -O or -f)"
msgstr ""
"Espandi le tabulazioni a spazi bianchi mentre stampi i risultati della\n"
"    procedura di sincronizzazione (utile solo con le opzioni -O e -f)"

#: options.c:164
msgid "Treat both files as binary files (only meaningful under Doz/Windoz)"
msgstr "Tratta entrambi i file come binari (utile solo sotto Doz/Windoz)"

#: options.c:166
msgid "Redirect warning and error messages from stderr to the indicated file"
msgstr ""
"Reindirizza avvertimenti e messaggi di errore\n"
"    dallo standard error (schermo)  al file indicato"

#: options.c:168
msgid "Redirect output from stdout to the indicated file"
msgstr "Reindirizza l'output dallo standard output (schermo)  al file indicato"

#: options.c:169
msgid "Show help message and predefined settings"
msgstr "Mostra questo messaggio di aiuto e le impostazioni predefinite"

#: options.c:170
msgid "Show version number, Copyright, Distribution Terms and NO-Warranty"
msgstr ""
"Mostra numero di versione, Copyright,\n"
"    termini di distribuzione e NON-Garanzia"

#: options.c:172
msgid ""
"\n"
"  Default numeric format (for both files to compare):\n"
msgstr ""
"\n"
"  Formato numerico predefinito (per entrambi i file da confrontare):\n"

#: options.c:173
#, c-format
msgid "Currency name = \"%s\"\n"
msgstr "Nome della valuta = \"%s\"\n"

#: options.c:174
#, c-format
msgid "Decimal point = `%c'\n"
msgstr "Punto decimale = `%c'\n"

#: options.c:176 options.c:955 options.c:986
#, c-format
msgid "Thousands separator = `%c'\n"
msgstr "Separatore delle migliaia = `%c'\n"

#: options.c:177
#, c-format
msgid "Number of digits in each thousands group = %u\n"
msgstr "Numero di cifre in ogni gruppo di migliaia = %u\n"

#: options.c:179 options.c:956 options.c:987
#, c-format
msgid "Leading positive sign = `%c'\n"
msgstr "Segno iniziale per numeri positivi = `%c'\n"

#: options.c:180 options.c:957 options.c:988
#, c-format
msgid "Leading negative sign = `%c'\n"
msgstr "Segno iniziale per numeri negativi = `%c'\n"

#: options.c:181 options.c:958 options.c:989
#, c-format
msgid "Prefix for decimal exponent = `%c'\n"
msgstr "Prefisso per esponente decimale = `%c'\n"

#: options.c:182 options.c:961 options.c:992
#, c-format
msgid ""
"Symbol used to denote the imaginary unit = `%c'\n"
"\n"
msgstr ""
"Simbolo usato per indicare l'unità immaginaria = `%c'\n"
"\n"

#: options.c:467 options.c:513 options.c:528 options.c:545 options.c:560
#: options.c:582 options.c:804 options.c:831 options.c:874 options.c:889
#, c-format
msgid "%s: invalid argument after `-%c' option\n"
msgstr "%s: un argomento non valido segue l'opzione `-%c'\n"

#: options.c:600 options.c:614 options.c:634 options.c:667 options.c:681
#: options.c:701
#, c-format
msgid "%s: memory exhausted\n"
msgstr "%s: memoria esaurita\n"

#: options.c:642 options.c:708 options.c:726 options.c:733 options.c:745
#: options.c:752 options.c:789
#, c-format
msgid "%s: invalid argument after `-%c' option:\n"
msgstr "%s: un argomento non valido segue l'opzione `-%c':\n"

#: options.c:644
#, c-format
msgid ""
"  The list of field delimiters can not be empty and\n"
"  must always include the newline character ('\\n')\n"
msgstr ""
"  La lista di separatori di campo non può essere vuota\n"
"  e deve sempre includere il carattere di nuova linea ('\\n')\n"

#: options.c:710
#, c-format
msgid ""
"  The list of field delimiters cannot be empty and\n"
"  must always include the newline string (\"\\n\").\n"
"  Care that the newline character cannot appear\n"
"  in any other delimiter than the newline string\n"
msgstr ""
"  La lista di separatori di campo non può essere vuota\n"
"  e deve sempre includere la stringa di nuova linea (\"\\n\").\n"
"  Inoltre, il carattere di nuova linea non può\n"
"  apparire in alcun altro separatore di campo\n"

#: options.c:728 options.c:747
#, c-format
msgid "  The format specification has not been respected\n"
msgstr "  La specifica di formato non è stata rispettata\n"

#: options.c:735 options.c:754
#, c-format
msgid "  The specified ranges do not have the same length\n"
msgstr "  Gli intervalli specificati non hanno la stessa lunghezza\n"

#: options.c:791
#, c-format
msgid "  you have missed to specify the currency name\n"
msgstr "  hai dimenticato di specificare il nome della valuta\n"

#: options.c:900 options.c:909
#, c-format
msgid "%s: cannot open file \"%s\":\n"
msgstr "%s: impossibile aprire il file \"%s\":\n"

#: options.c:951
#, c-format
msgid "The numeric format specified for the first file is illegal,\n"
msgstr "Il formato numerico specificato per il primo file è inaccettabile,\n"

#: options.c:953 options.c:984
#, c-format
msgid ""
"the following symbols should be all different\n"
"while two or more of them are actually equal:\n"
msgstr ""
"i simboli seguenti dovrebbero essere tutti diversi\n"
"mentre due o più di essi sono in realtà uguali:\n"

#: options.c:954 options.c:985
#, c-format
msgid ""
"\n"
"Decimal point = `%c'\n"
msgstr ""
"\n"
"Punto decimale = `%c'\n"

#: options.c:968
#, c-format
msgid "The numeric format specified for the first file is illegal:\n"
msgstr "Il formato numerico specificato per il primo file è inaccettabile:\n"

#: options.c:970 options.c:1001
#, c-format
msgid "the name of the currency may not contain digits,\n"
msgstr "il nome della valuta non può contenere cifre numeriche,\n"

#: options.c:972 options.c:1003
#, c-format
msgid "the symbol for the leading positive sign (`%c'),\n"
msgstr "il prefisso per numeri positivi (`%c'),\n"

#: options.c:974 options.c:1005
#, c-format
msgid "the symbol for the leading negative sign (`%c'),\n"
msgstr "il prefisso per numeri negativi (`%c'),\n"

#: options.c:976 options.c:1007
#, c-format
msgid "the decimal point (`%c'), or the thousands separator (`%c')\n"
msgstr "il punto decimale (`%c'), o il separatore delle migliaia (`%c')\n"

#: options.c:982
#, c-format
msgid "The numeric format specified for the second file is illegal,\n"
msgstr "Il formato numerico specificato per il secondo file è inaccettabile,\n"

#: options.c:999
#, c-format
msgid "The numeric format specified for the second file is illegal:\n"
msgstr "Il formato numerico specificato per il secondo file è inaccettabile:\n"

#: thrlist.c:341
#, c-format
msgid "Fatal error occurred during comparison of two numerical fields\n"
msgstr ""
"Si è verificato un errore inemendabile durante\n"
"il confronto di due campi numerici\n"

#: xalloc-die.c:35
msgid "memory exhausted"
msgstr "memoria esaurita"
